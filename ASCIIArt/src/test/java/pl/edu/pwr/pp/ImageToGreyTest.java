package pl.edu.pwr.pp;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class ImageToGreyTest {

	@Mock private BufferedImage image;
	int [][] picture;


	@Before
	public void setUp() {
	MockitoAnnotations.initMocks(this);
	picture = new int[1][1];
	picture[0][0]=195;
	}
	
	@Test
	public void shouldRescaleImageWithPreservedRatio() {
	Mockito.when(image.getWidth()).thenReturn(300);
	Mockito.when(image.getHeight()).thenReturn(150);
	BufferedImage result = ImageConverter.resizeImage(image, BufferedImage.TYPE_BYTE_GRAY, 100);
	Assert.assertThat(result.getHeight(), Matchers.is(Matchers.equalTo(50)));
	Mockito.verify(image).getWidth();
	Mockito.verify(image).getHeight();
	}
	
	
	private Path getPathToFile(String fileName) throws URISyntaxException {
 		URI uri = ClassLoader.getSystemResource(fileName).toURI();
 		return Paths.get(uri);
 	}
	
	@Test
	public void shouldReturnIntensitiesToAscii(){
		char[][] asciiArt= ImageConverter.intensitiesToAscii(picture);
		char znakAscii=':';
		Assert.assertThat(asciiArt[0][0], Matchers.is(Matchers.equalTo(znakAscii)));
	}
	
	@Test
	public void shouldBufferedImageConvertToImageWithTheRightWidth(){
		BufferedImage image = ImageConverter.ConvertToImage(picture, 1, 1);
		Assert.assertThat(image.getWidth(), Matchers.is(Matchers.equalTo(1)));
	}
}
