package pl.edu.pwr.pp;

import java.awt.EventQueue;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import java.awt.Font;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;

public class GraficznyInterfejsikUzytkownika extends JFrame implements ActionListener {

	private JFrame ramka;
	private JLabel lblObrazuu;
	private Ladowanko ladowanko;
	private ImageFileReader czytajPeGieEm;
	private JButton btnZapisak;
	private int[][] obrazeczek;

	private BufferedImage obrazeczekDoAscii;
	boolean czySzaroburo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GraficznyInterfejsikUzytkownika window = new GraficznyInterfejsikUzytkownika();
					window.ramka.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GraficznyInterfejsikUzytkownika() {
		incka();
	}

	/**
	 * incka the contents of the ramka.
	 */
	private void incka() {
		ramka = new JFrame();
		ramka.setBounds(100, 100, 800, 600);
		ramka.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// ############################################
		// ########### WCZYTAJ
		JButton btnLoadzik = new JButton("Wczytaj obraz");
		btnLoadzik.addActionListener(this);

		ramka.getContentPane().setLayout(null);
		btnLoadzik.setEnabled(true);
		btnLoadzik.setBackground(new java.awt.Color(244, 204, 204));
		btnLoadzik.setBounds(8, 50, 175, 50);
		ramka.getContentPane().add(btnLoadzik);

		// ############################################
		// ########### OPCJA 1
		JButton btnOpcyjka1 = new JButton("Opcja Pierwsza");

		btnOpcyjka1.addActionListener(e -> {
		});

		btnOpcyjka1.setEnabled(false);
		btnOpcyjka1.setBackground(new java.awt.Color(255, 242, 205));
		btnOpcyjka1.setBounds(20, 125, 150, 25);
		ramka.getContentPane().add(btnOpcyjka1);

		// ############################################
		// ########### OPCJA 2
		JButton btnOpcyjka2 = new JButton("Opcja Druga");
		btnOpcyjka2.addActionListener(e -> {
		});

		btnOpcyjka2.setEnabled(false);
		btnOpcyjka2.setBackground(new java.awt.Color(255, 242, 205));
		btnOpcyjka2.setBounds(20, 175, 150, 25);
		ramka.getContentPane().add(btnOpcyjka2);

		// ############################################
		// ########### ZAPIS
		btnZapisak = new JButton("Zapisz do pliku");
		btnZapisak.addActionListener(e -> {

			JFileChooser fileChooser = new JFileChooser();
			int retrival = fileChooser.showSaveDialog(null);
			if (retrival == JFileChooser.APPROVE_OPTION) {

				ImageFileWriter writer = new ImageFileWriter();
				BufferedImage newImage = new BufferedImage(obrazeczekDoAscii.getWidth(), obrazeczekDoAscii.getHeight(),
						obrazeczekDoAscii.TYPE_BYTE_GRAY);
				newImage = ImageConverter.resizeImage(obrazeczekDoAscii, obrazeczekDoAscii.TYPE_BYTE_GRAY,
						obrazeczekDoAscii.getWidth());
				obrazeczek = ImageConverter.ConvertToGray(newImage, czySzaroburo);

				writer.saveToTxtFile(ImageConverter.intensitiesToAscii(obrazeczek), fileChooser.getSelectedFile() + "");
			}
		});
		
		btnZapisak.setEnabled(false);
		btnZapisak.setBackground(new java.awt.Color(244, 204, 204));
		btnZapisak.setBounds(8, 225, 175, 50);
		ramka.getContentPane().add(btnZapisak);

		// ############################################
		// ########### FUNKCJA 1
		JButton btnFunkcyjka1 = new JButton("Funkcja Pierwsza");
		btnFunkcyjka1.addActionListener(e -> {
		});
		
		btnFunkcyjka1.setEnabled(false);
		btnFunkcyjka1.setBackground(new java.awt.Color(255, 242, 205));
		btnFunkcyjka1.setBounds(20, 300, 150, 25);
		ramka.getContentPane().add(btnFunkcyjka1);

		// ############################################
		// ########### FUNKCJA 2
		JButton btnFunkcyjka2 = new JButton("Funkcja Druga");
		btnFunkcyjka2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO: func2
			}
		});
		btnFunkcyjka2.setEnabled(false);
		btnFunkcyjka2.setBackground(new java.awt.Color(255, 242, 205));
		btnFunkcyjka2.setBounds(20, 350, 150, 25);
		ramka.getContentPane().add(btnFunkcyjka2);

		// ############################################
		// ########### MIEJSCE NA OBRAZECZEK
		JPanel panelisko = new JPanel();
		panelisko.setBounds(300, 30, 400, 400);
		panelisko.setBackground(new java.awt.Color(244, 204, 204));
		Border blackline;
		blackline = BorderFactory.createLineBorder(Color.black);
		panelisko.setBorder(blackline); 
		ramka.getContentPane().add(panelisko);
		panelisko.setLayout(null);

		// UUUUKKKKKRYYCIUUUCHHH BLOOOKEEEEER1
		lblObrazuu = new JLabel("UKRYCIUCH");
		lblObrazuu.setBounds(64, 54, 276, 315);
		panelisko.add(lblObrazuu);
		lblObrazuu.setBackground(new java.awt.Color(255, 242, 205));

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if (ladowanko == null)
			ladowanko = new Ladowanko(this);
		ladowanko.setVisible(true);
		if (ladowanko.czyOkiDoki()) {
			czytajPeGieEm = new ImageFileReader();
			obrazeczek = null;
			if (ladowanko.opcyjka == "plik") {
				try {
					obrazeczek = czytajPeGieEm.czytajPeGieEmFile(ladowanko.dajSciezyne());
					if (czytajPeGieEm.isPgm) {
						czySzaroburo = true;
						btnZapisak.setEnabled(true);
						obrazeczekDoAscii = ImageConverter.ConvertToImage(obrazeczek, czytajPeGieEm.width,
								czytajPeGieEm.height);
						lblObrazuu.setIcon(new ImageIcon(obrazeczekDoAscii.getScaledInstance(lblObrazuu.getWidth(),
								lblObrazuu.getHeight(), Image.SCALE_SMOOTH)));
					} else {
						czySzaroburo = false;
						btnZapisak.setEnabled(true);
						try {
							obrazeczekDoAscii = ImageIO.read(new File(ladowanko.dajSciezyne()));
							lblObrazuu.setIcon(new ImageIcon(obrazeczekDoAscii.getScaledInstance(lblObrazuu.getWidth(),
									lblObrazuu.getHeight(), Image.SCALE_SMOOTH)));
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				btnZapisak.setEnabled(true);
				try {
					if (ladowanko.dajSciezyne().contains("pgm")) {
						czySzaroburo = true;
						obrazeczek = czytajPeGieEm.czytajPeGieEmUrl(new URL(ladowanko.dajSciezyne()));
						obrazeczekDoAscii = ImageConverter.ConvertToImage(obrazeczek, czytajPeGieEm.width,
								czytajPeGieEm.height);
						lblObrazuu.setIcon(new ImageIcon(obrazeczekDoAscii.getScaledInstance(lblObrazuu.getWidth(),
								lblObrazuu.getHeight(), Image.SCALE_SMOOTH)));
					} else {
						czySzaroburo = false;
						try {
							obrazeczekDoAscii = ImageIO.read(new URL(ladowanko.dajSciezyne()));
							lblObrazuu.setIcon(new ImageIcon(obrazeczekDoAscii.getScaledInstance(lblObrazuu.getWidth(),
									lblObrazuu.getHeight(), Image.SCALE_SMOOTH)));
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							JOptionPane.showMessageDialog(null, "B��dy adres URL", "Warning ",
									JOptionPane.WARNING_MESSAGE);
						}
					}
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
	}
}
