package pl.edu.pwr.pp;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.util.stream.IntStream;

public class ImageConverter {

	/**
	 * Znaki odpowiadaj�ce kolejnym poziomom odcieni szaro�ci - od czarnego (0)
	 * do bia�ego (255).
	 */
	public static String INTENSITY_2_ASCII = "@%#*+=-:. ";

	/**
	 * Metoda zwraca znak odpowiadaj�cy danemu odcieniowi szaro�ci. Odcienie
	 * szaro�ci mog� przyjmowa� warto�ci z zakresu [0,255]. Zakres jest dzielony
	 * na r�wne przedzia�y, liczba przedzia��w jest r�wna ilo�ci znak�w w
	 * {@value #INTENSITY_2_ASCII}. Zwracany znak jest znakiem dla przedzia�u,
	 * do kt�rego nale�y zadany odcie� szaro�ci.
	 * 
	 * 
	 * @param intensity
	 *            odcie� szaro�ci w zakresie od 0 do 255
	 * @return znak odpowiadaj�cy zadanemu odcieniowi szaro�ci
	 */
	public static char intensityToAscii(int intensity) {
		int index = intensity * INTENSITY_2_ASCII.length() / 256;
		return INTENSITY_2_ASCII.charAt(index);
	}

	/**
	 * Metoda zwraca dwuwymiarow� tablic� znak�w ASCII maj�c dwuwymiarow�
	 * tablic� odcieni szaro�ci. Metoda iteruje po elementach tablicy odcieni
	 * szaro�ci i dla ka�dego elementu wywo�uje {@ref #intensityToAscii(int)}.
	 * 
	 * @param intensities
	 *            tablica odcieni szaro�ci obrazu
	 * @return tablica znak�w ASCII
	 */
	public static char[][] intensitiesToAscii(int[][] intensities) {
		// LELUM POLELUM KOLEGO!!!
		final char[][] asciiArt = new char[intensities.length][];
		IntStream.range(0, intensities.length).forEach(i -> asciiArt[i] = new char[intensities[i].length]);

		for (int i = 0; i < intensities.length; i++) {
			for (int j = 0; j < intensities[i].length; j++) {
				asciiArt[i][j] = intensityToAscii(intensities[i][j]);
			}
		}

		return asciiArt;
	}

	public static BufferedImage ConvertToImage(int[][] obrazeczek, int w, int h) {
		BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_BYTE_GRAY);
		WritableRaster raster = image.getRaster();
		for (int y = 0; y < h; y++) {
			for (int x = 0; (x < w); x++) {
				raster.setSample(x, y, 0, obrazeczek[y][x]);
			}
		}
		return image;
	}

	public static int[][] ConvertToGray(BufferedImage image, boolean czySzaroburo) {
		int[][] intensities = null;
		// inicjalizacja tablicy na odcienie szaro�ci
		intensities = new int[image.getHeight()][];
		for (int i = 0; i < image.getHeight(); i++) {
			intensities[i] = new int[image.getWidth()];
		}

		for (int i = 0; i < image.getHeight(); i++) {
			for (int j = 0; j < image.getWidth(); j++) {
				Color color = new Color(image.getRGB(j, i));
				if (czySzaroburo)
					intensities[i][j] = color.getRed();
				else
					intensities[i][j] = (int) (0.2989 * color.getRed() + 0.5870 * color.getGreen()
							+ 0.1140 * color.getBlue());
			}
		}

		return intensities;
	}

	public static BufferedImage resizeImage(BufferedImage originalImage, int type, int w) {
		int h = w * originalImage.getHeight() / originalImage.getWidth();
		BufferedImage resizedImage = new BufferedImage(w, h, type);
		Graphics2D g = resizedImage.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.drawImage(originalImage, 0, 0, w, h, null);
		g.dispose();
		return resizedImage;
	}

}
