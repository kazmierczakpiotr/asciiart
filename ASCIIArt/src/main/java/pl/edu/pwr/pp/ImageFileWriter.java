package pl.edu.pwr.pp;

import java.io.BufferedReader;

import java.io.PrintWriter;
import java.io.File;
import java.io.IOException;

public class ImageFileWriter {

	public void saveToTxtFile(char[][] ascii, String fileName) {
		// np. korzystając z java.io.PrintWriter
		try (PrintWriter elementToWrite = new PrintWriter(fileName)){
			
			for (int i = 0; i < ascii.length; i++) {
				for (int j = 0; j < ascii[i].length; j++) {
					elementToWrite.print(ascii[i][j]);
				}
				elementToWrite.println();// nowa linia ?
			}
			//elementToWrite.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
