package pl.edu.pwr.pp;

import javax.swing.JFrame;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;

public class Ladowanko extends JDialog implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JFrame ramka;
	private JTextField txtZapiskaUrLa;
	private JTextField txtPliczka;
	public String sciezyna;
	public String opcyjka;
	private ButtonGroup buttonGroup;
	public boolean okiDoki;
	private JButton btnLadowanka;

	/**
	 * Create the application.
	 */
	public Ladowanko(JFrame owner) {
		super(owner, "Ladowanko", true);
		initialize();
	}

	public String dajSciezyne() {
		return sciezyna;
	}

	public boolean czyOkiDoki() {
		return okiDoki;
	}

	public String getSelectedButtonText(ButtonGroup buttonGroup) {
		for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
			AbstractButton button = buttons.nextElement();

			if (button.isSelected()) {
				return button.getText();
			}
		}

		return null;
	}

	/**
	 * Initialize the contents of the ramka.
	 */
	private void initialize() {
		sciezyna = null;
		opcyjka = null;
		
		setBounds(100, 100, 596, 206);
		getContentPane().setLayout(null);

		JRadioButton rdbtnLadowankaUrl = new JRadioButton("z URL");
		rdbtnLadowankaUrl.setBounds(6, 73, 80, 23);
		getContentPane().add(rdbtnLadowankaUrl);

		JRadioButton rdbtnLadowankaPlik = new JRadioButton("z Pliku");
		rdbtnLadowankaPlik.setBounds(6, 101, 80, 23);
		rdbtnLadowankaPlik.setSelected(true);
		getContentPane().add(rdbtnLadowankaPlik);

		buttonGroup = new ButtonGroup();
		buttonGroup.add(rdbtnLadowankaPlik);
		buttonGroup.add(rdbtnLadowankaUrl);

		txtZapiskaUrLa = new JTextField();
		txtZapiskaUrLa.setBounds(92, 74, 477, 20);
		getContentPane().add(txtZapiskaUrLa);
		txtZapiskaUrLa.setColumns(10);

		txtPliczka = new JTextField();
		txtPliczka.setEditable(false);
		txtPliczka.setBounds(92, 102, 232, 20);
		getContentPane().add(txtPliczka);
		txtPliczka.setColumns(10);

		btnLadowanka = new JButton("Wczytaj");
		btnLadowanka.setBackground(new java.awt.Color(244, 204, 204));
		btnLadowanka.addActionListener(this);
		btnLadowanka.setBounds(334, 135, 148, 23);
		getContentPane().add(btnLadowanka);

		JButton btnAnuluj = new JButton("Anuluj");
		btnAnuluj.setBackground(new java.awt.Color(244, 204, 204));
		btnAnuluj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnAnuluj.setBounds(492, 135, 77, 23);
		getContentPane().add(btnAnuluj);

		JButton btnPrzegladaj = new JButton("Przegladaj");
		btnPrzegladaj.setBackground(new java.awt.Color(244, 204, 204));
		btnPrzegladaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				int returnVal = chooser.showOpenDialog(ramka);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					txtPliczka.setText(chooser.getSelectedFile().getAbsolutePath());

				}
			}
		});
		btnPrzegladaj.setBounds(334, 101, 235, 23);
		getContentPane().add(btnPrzegladaj);

		JLabel lblWczytajZ = new JLabel("Wczytaj obraz z: ");
		lblWczytajZ.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblWczytajZ.setBounds(92, 18, 340, 45);
		getContentPane().add(lblWczytajZ);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Object z = e.getSource();
		if (z == btnLadowanka) {
			okiDoki = true;
			if (getSelectedButtonText(buttonGroup) == "z Pliku") {
				sciezyna = txtPliczka.getText();
				opcyjka = "plik";
			} else {
				sciezyna = txtZapiskaUrLa.getText();
				opcyjka = "url";
			}
		} else
			okiDoki = false;

		this.setVisible(false);

	}
}
