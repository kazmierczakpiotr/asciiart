package pl.edu.pwr.pp;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.IntStream;

public class ImageFileReader {

	/**
	 * Metoda czyta plik pgm i zwraca tablic� odcieni szaro�ci.
	 * 
	 * @param fileName
	 *            nazwa pliku pgm
	 * @return tablica odcieni szaro�ci odczytanych z pliku
	 * @throws URISyntaxException
	 *             je�eli plik nie istnieje
	 */
	public int width;
	public int height;
	public boolean isPgm;

	public int[][] czytajPeGieEmUrl(URL url) {
		int[][] intensities = null;
		try {
			intensities = readPicture(new InputStreamReader(url.openStream()));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return intensities;
	}

	public int[][] czytajPeGieEmFile(String fileName) throws URISyntaxException {
		int[][] intensities = null;
		try {
			intensities = readPicture(new FileReader(fileName));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return intensities;
	}

	private int[][] readPicture(Reader fileName) {
		int columns = 0;
		int rows = 0;
		int[][] intensities = null;

		try (BufferedReader reader = new BufferedReader(fileName)) {
			// w kolejnych liniach kodu mo�na / nale�y skorzysta� z metody
			// reader.readLine()

			// pierwsza linijka pliku pgm powinna zawiera� P2
			String magicNumber = reader.readLine();

			if (magicNumber.equals("P2")) {
				// System.err.println("Nieprawidlowy format pliku: " +
				// fileName);
				isPgm = true;
			} else
				return null;

			// druga linijka pliku pgm powinna zawiera� komentarz rozpoczynaj�cy
			// si� od #
			String comment = reader.readLine();
			if (comment.charAt(0) == '#') {
				isPgm = true;
			} else
				return null;

			// trzecia linijka pliku pgm powinna zawiera� dwie liczby - liczb�
			// kolumn i liczb� wierszy (w tej kolejno�ci). Te warto�ci nale�y
			// przypisa� do zmiennych columns i rows.
			String[] dimension = reader.readLine().split(" ");
			try {
				columns = Integer.parseInt(dimension[0]);
				width = columns;
				rows = Integer.parseInt(dimension[1]);
				height = rows;
			} catch (NumberFormatException e) {
				System.err.println("Nieprawidlowy format pliku");
			}
			// czwarta linijka pliku pgm powinna zawiera� 255 - najwy�sz�
			// warto�� odcienia szaro�ci w pliku
			try {
				Integer.parseInt(reader.readLine());
			} catch (NumberFormatException e) {
				System.err.println("Nieprawidlowy format pliku");
			}

			// inicjalizacja tablicy na odcienie szaro�ci
			intensities = new int[rows][];

			for (int i = 0; i < rows; i++) {
				intensities[i] = new int[columns];
			}

			
//			IntStream.range(0,rows).forEach(
//					i -> intensities[i] = new int[columns]);
			
			// kolejne linijki pliku pgm zawieraj� odcienie szaro�ci kolejnych
			// pikseli rozdzielone spacjami
			String line = null;
			int currentRow = 0;
			int currentColumn = 0;
			while ((line = reader.readLine()) != null) {
				String[] elements = line.split(" ");
				for (int i = 0; i < elements.length; i++) {
					if (currentColumn == columns) {
						currentColumn = 0;
						currentRow++;
					}
					intensities[currentRow][currentColumn] = Integer.parseInt(elements[i]);
					currentColumn++;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return intensities;
	}

}
